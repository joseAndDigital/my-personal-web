export const linkedinProfile = "https://www.linkedin.com/in/josemartinezdeveloper/";
export const contactMail = "contact@josemartinezdev.com";

export const dotNet = ".Net";
export const web = "Web";
export const android = "Android";
export const dotNetTechs = ".Net | Visual Studio | C# | ASP .Net";
export const webTechs = "React | JavaScript | Node.js | Visual Studio Code | HTML | CSS";
export const androidTechs = "Kotlin | Android Studio | React Native | Visual Studio Code";

///////Portfolio card types
export const weatherApp = "WEATHER_APP";
export const businessApp = "BUSINESS_APP";
export const theEditApp = "THEEDIT_APP";

export const weatherAppLink =
  "https://play.google.com/store/apps/details?id=com.app.totana_weather_app";
export const businessAppLink =
  "https://play.google.com/store/apps/details?id=com.josetotana.totanabusiness";
export const theEditAppLink = "https://play.google.com/store/apps/details?id=com.josemdev.theedit";

////////////////// breakpoints
export const mobileBreakpoint = "768px";
