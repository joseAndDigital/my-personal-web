import React from "react";
import "./Footer.scss";

const Footer = () => {
  return (
    <footer className="footer-section">
      © 2021 por Jose Martinez. Todos los derechos reservados.
    </footer>
  );
};

export default Footer;
